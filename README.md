_Forked from https://github.com/bigclownlabs/bch-usb-gateway_

# Turris Omnia

## Install requirements
```
opkg update
opkg install kmod-usb-acm python3 python3-pip python3-pyyaml
pip3 install docopt paho-mqtt pyserial simplejson
```

# Turris blue
```
opkg update
opkg install kmod-usb2 kmod-usb-ohci kmod-usb-serial-ftdi
```

## Install the gateway from git
```
opkg install git-http
git clone https://gitlab.labs.nic.cz/mvician/bigclown-gateway.git /srv/bigclown-gateway
ln -s /srv/bigclown-gateway/bc-gateway.py /usr/bin/bc-gateway
ln -s /srv/bigclown-gateway/bc-gateway.init /etc/init.d/bc-gateway
cp /srv/bigclown-gateway/bc-gateway.luci.example /etc/config/bc-gateway # And update it
/etc/init.d/bc-gateway enable
/etc/init.d/bc-gateway start
```

## Enable SSL

```
opkg install ca-certificates
# Find which certificate in /etc/ssl/certs/ issued your certificate or put there yours CA file
openssl s_client -connect YOURSERVER:8883
# Verify it as
openssl s_client -connect YOURSERVER:8883 -CAfile /etc/ssl/certs/DISCOVEREDCERT.crt
```

## For debug

opkg install mosquitto-client

## Verify autostart

```
ls /etc/rc*.d/*bc-gateway
```
