omnia:
	opkg update
	opkg install kmod-usb-acm python3 python3-pip
	pip3 install docopt paho-mqtt pyserial simplejson

ubuntu:
	echo "install ubuntu"
	sudo apt install python3 python3-pip
	sudo pip3 install docopt paho-mqtt pyserial simplejson
	# Copy your certifiacate to /usr/local/share/ca-certificates
	# It has to end with .crt extension! but it could be pem format

run:
	sudo ./bc-gateway.py -D -c bigclown.yaml
